# Use an official Python runtime as a base image
FROM python:3.8-slim-buster

# Set the working directory
WORKDIR /app

# Copy the Python scripts and requirements file to the container
COPY Assignment_PartA.py Assignment_PartB.py requirements.txt  /app/

# Add the data directory into the container
ADD data /app/data

# Install any necessary dependencies
RUN pip install --upgrade scikit-learn
RUN pip install -r requirements.txt

# Set the default command to run your scripts
CMD ["python", "Assignment_PartB.py"]  # Modify this to run the script you want by default

