# Predictive Modelling Assignment

This repository contains code and resources for the "Predictive Modelling" assignment. Split the assignment into two parts: Part A involves importing and understanding data, while Part B focuses on predictive modelling.

## Content Structure

1. **Part A - Importing and Understanding Data**:
    Exploratory Data Analysis (EDA) insights and visualizations.
    Plotting unique cuisines, top suburbs with the highest number of restaurants, and correlating  cost with rating.
   - [Assignment_PartA.ipynb](Assignment_PartA.ipynb): Jupyter Notebook for data import and analysis.
   - [Assignment_PartA.py](Assignment_PartA.py): Python script corresponding to Part A.

2. **Part B - Predictive Modelling**:
    Feature Engineering
    Data cleaning and encoding for predictive modelling.
    Regression
    Linear regression models (model_regression_1 and model_regression_2).
    Mean Squared Error (MSE) analysis on test data.
    Classification
    Logistic regression model (model_classification_3) for binary classification.
    Confusion matrix analysis and observations.
   - [Assignment_PartB.ipynb](Assignment_PartB.ipynb): Jupyter Notebook for predictive modelling.
   - [Assignment_PartB.py](Assignment_PartB.py): Python script corresponding to Part B.

3. **Docker Configuration**:
   - [Dockerfile](Dockerfile): Dockerfile for creating a Docker image to run the scripts.
   - [requirements.txt](requirements.txt): Python dependencies for the scripts.

4. **Data and Resources**:
   - [data/](data/): Directory containing datasets and necessary data files.
   - [zomato_df_final_data.hyper](zomato_df_final_data.hyper): Tableau file for the final dataset.

## Usage

- **Jupyter Notebooks**: Explore the assignment using Jupyter Notebooks.
- **Python Scripts**: Run the provided Python scripts using an appropriate Python environment.
- **Docker Image**: Build a Docker image using the provided Dockerfile to encapsulate and run the scripts.

## Tableau Dashboard

Access the related Tableau Dashboard [Link to tableau dashboard](https://public.tableau.com/authoring/Assignment_DSTS/Dashboard1%231)

## To run the docker image

Docker Image Deployment Commands

      To pull the image from docker hub:
          docker pull aliasgeorge/assignment1_alias:latest
     To run the docker container:
          docker run aliasgeorge/assignment1_alias:latest


## Results and Reports

- [Assignment.pdf](Assignment.pdf): Contains results of the trained models on the test data for regression and classification tasks.

## Project Collaboration

- **GitLab Repository**: [Link to GitLab Repository](https://gitlab.com/AliasG/dst_assignment_predictive_modelling.git).
- **Docker Hub Image**: [Link to Docker Image on Docker Hub](https://hub.docker.com/repository/docker/aliasgeorge/assignment1_alias/general).

## Contributors

- [Alias George] - Main Contributor
- [Ibrahim] - Collaborator
