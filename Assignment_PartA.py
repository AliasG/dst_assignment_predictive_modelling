#!/usr/bin/env python
# coding: utf-8

# ### Problem Statement:  

# Predictive Modelling of Eating-Out problem

# ### Objectives: 

# Apply feature engineering,modelling and deployment knowledge to real-world dataset

# Working Code:

# In[24]:


# Import necessary libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import geopandas as gpd
from sklearn.preprocessing import LabelEncoder
from ast import literal_eval
from shapely.geometry import Point

# If needed, install geopandas
# !pip install geopandas


# In[3]:


# Load the dataset:
data_path = "data/zomato_df_final_data.csv"
df = pd.read_csv(data_path)
df.head()


# #### Exploratory data analysis 

# In[4]:


# data shape
print(df.shape)
# column names
print(df.columns)


# In[5]:


# summary of dataset
print(df.info())


# #### Plots And Graphs

# Qn 1.a How many unique cuisines are served by Sydney restaurants?

# In[6]:


# Convert the 'cuisine' column to a list of cuisines
df['cuisine'] = df['cuisine'].apply(lambda x: eval(x) if isinstance(x, str) else x)

# Flatten the list of cuisines (as each entry is a list of cuisines)
flat_cuisines = [cuisine for sublist in df['cuisine'] for cuisine in sublist]

# Count the number of unique cuisines
unique_cuisine_count = len(set(flat_cuisines))

# Plot the count of unique cuisines
plt.figure(figsize=(10, 3))
plt.barh(['Unique Cuisines'],[unique_cuisine_count], color='skyblue')
plt.xlabel('Count')
plt.title('Number of Unique Cuisines served by Sydney Restaurants')
# Display the number of unique cuisines
plt.annotate(f"Unique Cuisines: {unique_cuisine_count}", xy=(0.5, 0.5), xycoords="axes fraction", fontsize=15, ha="center")
plt.show()


# Qn 1.b Which suburbs (top-3) have the highest number of restaurants?

# In[9]:


# Count the occurrences of each suburb
suburb_counts = df['subzone'].value_counts().nlargest(3)

# Plotting
plt.figure(figsize=(8, 6))
sns.barplot(x=suburb_counts.index, y=suburb_counts.values, palette='viridis')
plt.xlabel('Suburb')
plt.ylabel('Count')
plt.title('Top 3 Suburbs with the Highest Number of Restaurants')

plt.tight_layout()
plt.show()

# Descriptive insights for top 3 suburbs with highest number of restaurants
print("Insights for top 3 suburbs with highest number of restaurants:")
print(suburb_counts)


# Qn:1.c “Restaurants with ‘excellent’ rating are mostly very expensive while those with ‘Poor’ rating are rarely expensive”. Do you agree on this statement or not? Please support your answer by numbers and visuals. (hint: use stacked bar chart or histogram to relate ‘cost’ to 'rating_text')

# In[13]:


# Drop rows with missing 'cost' or 'rating_text'
df.dropna(subset=['cost', 'rating_text'], inplace=True)

# Define cost bins
cost_bins = [0, 25, 50, 75, 100, 9999]
cost_labels = ['Very Low', 'Low', 'Moderate', 'High', 'Very High']

# Categorize 'cost' into bins
df['cost_category'] = pd.cut(df['cost'], bins=cost_bins, labels=cost_labels)

# Create a crosstab to calculate the proportions
cross_table = pd.crosstab(df['rating_text'], df['cost_category'], normalize='index')

# Plotting
plt.figure(figsize=(10, 6))
cross_table.plot(kind='bar', stacked=True, cmap='viridis')
plt.xlabel('Rating')
plt.ylabel('Proportion')
plt.title('Proportion of Ratings for Each Cost Category')
plt.xticks(rotation=0)
plt.legend(title='Cost Category', loc='upper left')

plt.tight_layout()
plt.show()


# Low cost cuisines are available more in average rating restaurants whereas very high cost cuisines are more in restaurants with excellent rating.

# Qn 2: Perform exploratory analysis for the variables of the data. This can be done by producing histograms and distribution plots and descriptive insights about these variables.This can be performed at least for the following variables.Cost,Rating,Type

# In[14]:


# Histogram for 'Cost'
plt.figure(figsize=(10, 6))
sns.distplot(df['cost'], bins=20, kde=True, color='green')
plt.xlabel('Cost (AUD)')
plt.ylabel('Frequency')
plt.title('Histogram of Cost')
plt.show()

# Descriptive insights for 'Cost'
print("Descriptive Statistics for 'Cost':")
print(df['cost'].describe())


# In[18]:


# Plot for rating
plt.figure(figsize=(10, 6))
sns.distplot(df['rating_number'], bins=20, kde=True, color='lightgreen')
plt.xlabel('Rating')
plt.ylabel('Frequency')
plt.title('Distribution of Rating')
plt.show()

# Descriptive insights for 'Rating'
print("Descriptive Statistics for 'Rating':")
print(df['rating_number'].describe())


# In[22]:


# Convert the 'type' column to a list of types
df['type'] = df['type'].apply(lambda x: eval(x) if isinstance(x, str) else x)

# Flatten the list of types (as each entry is a list of types)
flat_types = [t for sublist in df['type'] for t in sublist]

# Count the number of unique types
type_counts = pd.Series(flat_types).value_counts()

# Plot bar chart for 'Type'
plt.figure(figsize=(12, 6))
type_counts.plot(kind='bar', color=sns.color_palette('viridis'))
plt.xlabel('Type')
plt.ylabel('Count')
plt.title('Top Restaurant Types')
plt.xticks(rotation=45)  # Rotate x-axis labels for better visibility
plt.tight_layout()

# Show the plot
plt.show()

# Descriptive insights for 'Type'
print("Top 5 Restaurant Types:")
print(type_counts.head())


# Qn 3. Produce Cuisine Density Map: Using the restaurant geographic information and the provided “sydney.geojson” file, write a python function to show a cuisine density map where each suburb is colour-coded by the number of restaurants that serve a particular cuisine. This function can be called as: “show_cuisine_densitymap(cuisine='Indian')”.(Hint: use the spatial join in geopandas)
#    

# In[31]:


def show_cuisine_densitymap(cuisine, geojson_path='sydney.geojson', restaurant_data_path='zomato_df_final_data.csv', ax=None):
    # Load the Sydney map data
    sydney_map = gpd.read_file(geojson_path)

    # Load the restaurant data
    restaurant_data = pd.read_csv(restaurant_data_path)

    # Remove rows with NA values
    restaurant_data = restaurant_data.dropna(subset=['lat', 'lng'])

    # Plot the entire Sydney map
    if ax is None:
        fig, ax = plt.subplots(figsize=(12, 6))
    sydney_map.plot(ax=ax, alpha=0.5, edgecolor='k')

    # Filter data for the specified cuisine
    cuisine_data = restaurant_data[restaurant_data['cuisine'].str.contains(cuisine, case=False, na=False)]

    # Group by suburb and count the number of restaurants for the specified cuisine
    cuisine_counts = cuisine_data.groupby('subzone').size().reset_index(name='restaurant_count')

    # Merge with sydney_map to include the geometry
    cuisine_density_map = pd.merge(sydney_map, cuisine_counts, left_on='SSC_NAME', right_on='subzone', how='left')

    # Plot the cuisine density map on top
    cuisine_density_map.plot(column='restaurant_count', cmap='OrRd', linewidth=0.5, ax=ax, edgecolor='k', legend=True,
                             legend_kwds={'label': f'Number of {cuisine} Restaurants', 'orientation': 'vertical'})

    ax.set_title(f'Cuisine Density Map for {cuisine} cuisine', fontdict={'fontsize': 20, 'fontweight': 'bold'})
    ax.axis('off')

# Create subplots to show the maps side by side
fig, axes = plt.subplots(1, 2, figsize=(18, 6))

# Example usage: Show entire Sydney map and highlight 'Indian' cuisine
show_cuisine_densitymap(cuisine='Indian', ax=axes[0])
show_cuisine_densitymap(cuisine='Chinese', ax=axes[1])

plt.tight_layout()
plt.show()


# Both cuisines, Indian and Chinese, have a presence throughout Sydney, but they seem to be more concentrated in specific regions.
# Suburbs with higher density of corresponding restaurants are depicted in darker shades on the map.

# ##### Bonus Questions:

# In[32]:


import plotly.express as px
# use plotly to plot the graph
plt.figure(figsize=(10, 8))
fig3 = px.histogram(df, x='cost', color='rating_text',
                   title='Relationship Between Cost and Rating Text',
                   labels={'cost': 'Cost', 'rating_text': 'Rating Text'})
fig3.show()


# Non-interactive plots, like those generated using matplotlib, typically provide static images.These plots are good for basic visualization but lack interactivity.Plotly plots are interactive,we can hover over data points to see additional information,
# and select specific data to highlight and focus on.

# In[ ]:




