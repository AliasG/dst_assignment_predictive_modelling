geopandas==0.9.0
matplotlib==3.1.3
numpy==1.18.1
pandas==1.0.1
plotly==5.17.0
scikit-learn==0.24.2
seaborn==0.10.0
Shapely==2.0.1
