#!/usr/bin/env python
# coding: utf-8

# ## Part B - Predictive modelling

# In[65]:


#import libraries:

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression, SGDRegressor, LogisticRegression
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import classification_report, confusion_matrix, mean_squared_error, r2_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.preprocessing import LabelEncoder
from sklearn.neighbors import KNeighborsClassifier


# In[66]:


# Load the dataset:
data_path = "data/zomato_df_final_data.csv"
df = pd.read_csv(data_path)
df.head()


# ### Exploratory data analysis (EDA)

# In[67]:


# Display the shape of the dataset and column names
print("Data Shape:", df.shape)
print("Column Names:", df.columns)

# Display summary information about the dataset
print(df.info())

# Identify categorical variables
categorical = [var for var in df.columns if df[var].dtype=='O']
print("There are {} categorical variables\n".format(len(categorical)))
print("The categorical variables are: ", categorical)

# Display the first few rows of categorical variables
print(df[categorical].head())


# ###### Explore categorical variables

# In[68]:


# Explore categorical variables
df[categorical].head()


# ###### Perform data cleaning to remove/impute any records that are useless in the predictive task (such as NA, NaN, etc.)

# In[69]:


# Impute missing values in numerical and categorical columns
numerical_cols = df.select_dtypes(include='number').columns
categorical_cols = df.select_dtypes(include='object').columns

df[numerical_cols] = df[numerical_cols].fillna(df[numerical_cols].mean())
df[categorical_cols] = df[categorical_cols].fillna(df[categorical_cols].mode().iloc[0])

# List of columns to remove during feature extraction
columns_to_remove = ['address', 'phone', 'link', 'title', 'color', 'cuisine_color']
df.drop(columns_to_remove, axis=1, inplace=True)


# In[70]:


# Verify if there are any remaining missing values
df.isnull().sum()


# ##### Feature Engineering

# In[72]:


# Convert categorical columns to numeric using Label Encoding
categorical_cols_encoded = df.select_dtypes(include='object').columns
label_encoder = LabelEncoder()
df_encoded = df[categorical_cols_encoded].apply(lambda col: label_encoder.fit_transform(col))
df_encoded = pd.concat([df_encoded, df[numerical_cols]], axis=1)
# Assuming 'rating_number' is the target variable and other columns are predictors
Y = df_encoded['rating_number']  # Target variable
X = df_encoded.drop('rating_number', axis=1)  # Features


# In[73]:


X.columns


# ##### Regression

#  Qn. Build a linear regression model (model_regression_1) to predict the restaurants rating (numeric rating) from other features (columns) in the dataset. Please consider splitting the data into train (80%) and test (20%) sets.[Hint: please use sklearn.model_selection.train_test_split and set random_state=0 “while splitting]

# In[74]:


# Split the data into 80% training and 20% testing sets
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=0)

# Scale the features using Min-Max Scaling
scaler = MinMaxScaler()
X_train_scaled = scaler.fit_transform(X_train)  # Scaled training features
X_test_scaled = scaler.fit_transform(X_test)    # Scaled testing features

# Create and train the linear regression model using scaled features
model_regression_minmax = LinearRegression()
model_regression_minmax.fit(X_train_scaled, y_train)  # Train the model using scaled training features

# Predict on the test set using the model with Min-Max scaling
y_pred_minmax = model_regression_minmax.predict(X_test_scaled)

# Evaluate the model with Min-Max scaling
mse_model_1 = mean_squared_error(y_test, y_pred_minmax)
r_squared_model_1 = r2_score(y_test, y_pred_minmax)

# Print evaluation metrics for the model with Min-Max scaling
print('Mean Squared Error (Min-Max Scaling):', mse_model_1)
print('R-squared (Min-Max Scaling):', r_squared_model_1)


# Qn 4:Build another linear regression model (model_regression_2) with using the Gradient Descent as the optimisation function.

# In[75]:


# Create and train the linear regression model using Gradient Descent
model_regression_gradient_descent = SGDRegressor(max_iter=1000, tol=1e-3, random_state=0)
model_regression_gradient_descent.fit(X_train_scaled, y_train)  # Train the model using scaled training features

# Predict on the test set using the model with Gradient Descent
y_pred_gradient_descent = model_regression_gradient_descent.predict(X_test_scaled)

# Evaluate the model with Gradient Descent
mse_gradient_descent_model_2 = mean_squared_error(y_test, y_pred_gradient_descent)
r_squared_gradient_descent_model_2= r2_score(y_test, y_pred_gradient_descent)

# Print evaluation metrics for the model with Gradient Descent
print('Mean Squared Error (Gradient Descent):', mse_gradient_descent_model_2)
print('R-squared (Gradient Descent):', r_squared_gradient_descent_model_2)


# ###### Report the mean square error (MSE) on the test data for both models.

# In[76]:


# Print MSE for both models
print('MSE for Linear Regression (model_regression_1):', mse_model_1)
print('MSE for Linear Regression with Gradient Descent (model_regression_2):', mse_gradient_descent_model_2)


# In[81]:


X


# ### III. Classification:

# Qn 6:Simplify the problem into binary classifications where class 1 contains ‘Poor’ and ‘Average’ records while class 2 contains ‘Good’, ‘Very Good’ and ‘Excellent’ records.
# Qn 7:Build a logistic regression model (model_classification_3) for the simplified data, where training data is 80% and the test data is 20%.[Hint: please use sklearn.model_selection.train_test_split and set random_state=0 “while splitting]
# Qn 8. Use the confusion matrix to report the results of using the classification model on the test data.
# Qn 9. Draw your conclusions and observations about the performance of the model relevantto the classes’ distributions.

# In[82]:


# Create a new DataFrame for classification features
X_classification = X

# Assign binary classes based on the rating_text
X_classification['class'] = df['rating_text'].apply(lambda x: 1 if x in ['Poor', 'Average'] else 2)

# Features (X) and target (Y)
X = X_classification.drop(['class','rating_text'], axis=1)  # Features
Y = X_classification['class']  # Target variable

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=0)

# Create and train a logistic regression model
model_classification_3 = LogisticRegression()
model_classification_3.fit(X_train, y_train)

# Predict on the test set
y_pred = model_classification_3.predict(X_test)

# Evaluate the classification model using confusion matrix
conf_matrix = confusion_matrix(y_test, y_pred)

# Generate a classification report
report = classification_report(y_test, y_pred)

# Print the classification report
print('Classification Report:\n', report)


# ##### Confusion Matrix

# In[84]:


# Define class names
class_names = ['Poor/Average', 'Good/Very Good/Excellent']

# Plot the confusion matrix
plt.figure(figsize=(8, 6))
sns.heatmap(conf_matrix, annot=True, fmt='d', cmap='Blues', 
            xticklabels=class_names, yticklabels=class_names)
plt.xlabel('Predicted')
plt.ylabel('Actual')
plt.title('Confusion Matrix')
plt.show()


# ##### Observations

# Model performs well for Class 1 due to a larger number of instances.Class 2 predictions are satisfactory but could improve, especially recall.The data is imbalanced, affecting Class 2 predictions.

# ##### Bonus

# Qn: Repeat the previous classification task using three other models of your choice (example suggestions here (on Scikit-Learn website) and report the performance.

# In[37]:



# Define models
models = {
    'Logistic Regression': LogisticRegression(),
    'Random Forest': RandomForestClassifier(),
    'SVM': SVC(),
    'KNN': KNeighborsClassifier()
}

# Evaluate models
for name, model in models.items():
    # Train the model
    model.fit(X_train, y_train)
    
    # Predict on the test set
    y_pred = model.predict(X_test)
    
    # Print the model name
    print(f"\nModel: {name}")
    
    # Print classification report
    print(classification_report(y_test, y_pred))


# ##### Insights based on 4 models

# Random Forest performed exceptionally well, achieving a perfect accuracy, precision, recall, and F1-score for both classes. However, this could indicate overfitting.
# Logistic Regression also performed quite well with a good balance between precision, recall, and F1-score for both classes.
# SVM had high precision for class 1 but struggled to correctly classify class 2, leading to a significant drop in recall and F1-score for class 2.
# KNN had moderate performance, with a particular challenge in classifying class 2, resulting in lower recall and F1-score for class 2.
# In summary, Random Forest and Logistic Regression seem to be the best performers in this context, providing a good balance between precision, recall, and accuracy.

# In[ ]:




